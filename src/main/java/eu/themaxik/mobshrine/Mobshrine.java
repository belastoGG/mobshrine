/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.themaxik.mobshrine;

import eu.themaxik.mobshrine.events.MobSpawnEvent;
import eu.themaxik.mobshrine.events.PlayerCreatesShrineEvent;
import eu.themaxik.mobshrine.events.PlayerDropItem;
import eu.themaxik.mobshrine.events.ShrineDestroyEvent;
import eu.themaxik.mobshrine.util.ShrineManager;
import eu.themaxik.mobshrine.util.Ticker;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Max Dizendorf
 */
public class Mobshrine extends JavaPlugin {

    public ShrineManager shrineManager;
    private PluginManager pm;
    private Mobshrine instance;

    public void onEnable() {
        System.out.println("Starte Mobshrine");
        instance = this;
        pm = Bukkit.getPluginManager();

        loadConfig();
        shrineManager = new ShrineManager(this);
        shrineManager.loadOldShrines();

        registerEvents();
        registerCommands();
        new Ticker(this);
    }

    public void loadConfig() {
        getConfig().options().copyDefaults(true);
        getConfig().addDefault("radius", 100);
        getConfig().addDefault("lifetime", 168);
        saveConfig();
    }

    private void registerEvents() {
        pm.registerEvents(new PlayerDropItem(this), this);
        pm.registerEvents(new PlayerCreatesShrineEvent(this), this);
        pm.registerEvents(new MobSpawnEvent(shrineManager), this);
        pm.registerEvents(new ShrineDestroyEvent(shrineManager), this);
    }

    private void registerCommands() {
        this.getCommand("Shrines").setExecutor(this);
    }

}
