package eu.themaxik.mobshrine.util;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ShrineCreateEvent extends Event implements Cancellable {

    public static final HandlerList handlers = new HandlerList();
    public boolean cancelled = false;

    Location location;
    Item item;

    public ShrineCreateEvent(Location location, Item item) {
        this.location = location;
        this.item = item;
    }

    public Location getLocation() {
        return location;
    }

    public Item getItem() {
        return item;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        cancelled = true;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    static public HandlerList getHandlerList() {
        return handlers;
    }
}
