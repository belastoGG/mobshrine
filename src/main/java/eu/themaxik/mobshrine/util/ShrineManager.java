/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.themaxik.mobshrine.util;

import eu.themaxik.mobshrine.Mobshrine;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Max Dizendorf
 */
@Getter
public class ShrineManager {

    private Mobshrine mobshrine;
    private int radius;
    private static int radiusSquared;
    private ArrayList<Shrine> shrines = new ArrayList<>();

    public ShrineManager(Mobshrine mobshrine) {
        this.mobshrine = mobshrine;
        radius = mobshrine.getConfig().getInt("radius");
        radiusSquared = radius * radius;
    }

    public void manage(Shrine shrine) {
        if (!shrines.contains(shrine)) {
            shrines.add(shrine);
        }

        Location loc = shrine.loc;
        String shrineName = shrine.randomName;
        String path = "Shrine." + shrineName;

        mobshrine.getConfig().set(path + ".world", loc.getWorld().getName());
        mobshrine.getConfig().set(path + ".X", loc.getBlockX());
        mobshrine.getConfig().set(path + ".Y", loc.getBlockY());
        mobshrine.getConfig().set(path + ".Z", loc.getBlockZ());
        mobshrine.getConfig().set(path + ".end", shrine.endTime);
        mobshrine.saveConfig();
    }

    public void unManage(Shrine shrine) {
        if (shrines.contains(shrine)) {
            shrines.remove(shrine);
        }

        if (mobshrine.getConfig().contains("Shrine." + shrine.randomName)) {
            mobshrine.getConfig().set("Shrine." + shrine.randomName, null);
            mobshrine.saveConfig();
        }
    }

    public void loadOldShrines() {
        ConfigurationSection shrineConf = mobshrine.getConfig().getConfigurationSection("Shrine");
        if (shrineConf == null) return;

        shrineConf.getValues(false).keySet().forEach((String key) -> {
            int locX = shrineConf.getConfigurationSection(key).getInt("X");
            int locY = shrineConf.getConfigurationSection(key).getInt("Y");
            int locZ = shrineConf.getConfigurationSection(key).getInt("Z");
            String world = shrineConf.getConfigurationSection(key).getString("world");
            long endtime = shrineConf.getConfigurationSection(key).getLong("end");

            World w = Bukkit.getWorld(world);
            if (w == null) return;

            Shrine shrine = new Shrine();
            shrine.randomName = key;
            shrine.loc = new Location(Bukkit.getWorld(world), locX, locY, locZ);
            shrine.endTime = endtime;

            manage(shrine);
        });
    }

    public Shrine getShrine(Location loc) {
        for (int i = 0; i < shrines.size(); i++) {
            if (shrines.get(i).loc.equals(loc)) {
                return shrines.get(i);
            }
        }

        return null;
    }

    public boolean shrineExist(Location loc) {
        for (int i = 0; i < shrines.size(); i++) {
            if (shrines.get(i).loc.equals(loc)) {
                return true;
            }
        }

        return false;
    }

    public boolean isInReachOfShrine(Location location) {
        for (int i = 0; i < shrines.size(); i++) {
            if (shrines.get(i).loc.getWorld() == location.getWorld()) {
                if (shrines.get(i).loc.distanceSquared(location) < radiusSquared) {
                    return true;
                }
            }
        }

        return false;
    }

    public List<Shrine> getExpiredShrines() {
        List<Shrine> list = new ArrayList<>();
        for (Shrine shrine : shrines) {
            if (shrine.endTime < System.currentTimeMillis()) {
                list.add(shrine);
            }
        }

        return list;
    }
}
