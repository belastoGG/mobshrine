package eu.themaxik.mobshrine.util;

import eu.themaxik.mobshrine.Mobshrine;
import org.bukkit.scheduler.BukkitRunnable;

public class Ticker {

    private Mobshrine mobshrine;
    private boolean working;

    public Ticker(Mobshrine mobshrine) {
        this.mobshrine = mobshrine;
        StartTicker();
    }

    public void StartTicker() {
        if (working) return;

        working = true;
        new BukkitRunnable() {
            public void run() {
                mobshrine.shrineManager.getExpiredShrines().forEach((Shrine shrine) -> {
                    mobshrine.shrineManager.unManage(shrine);
                });
            }
        }.runTaskTimer(mobshrine, 0, 40).getTaskId();

        new BukkitRunnable() {
            public void run() {
                mobshrine.shrineManager.getShrines().forEach((Shrine shrine) -> {
                    shrine.playAnimation();
                });
            }
        }.runTaskTimer(mobshrine, 0, 1).getTaskId();
    }
}
