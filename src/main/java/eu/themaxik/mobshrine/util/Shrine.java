/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.themaxik.mobshrine.util;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.Particle;

/**
 * @author Max Dizendorf
 */
@Getter
@Setter
public class Shrine {

    public Location loc;
    public long endTime;
    public String randomName;
    /////Particle
    private Location particleLoc;
    private static final double PI_2 = Math.PI * 2;

    private static final double HEIGHT = 6; // Höhe der Helix;
    private static final double COLUMN_COUNT = 4; // Anzahl der "Säulen"
    private static final double ROUNDS_PER_BLOCK = 0.16666; // Umdrehungen pro Block
    private static final double PARTICLE_DEGREE = 8.0; // Alle X Grad ein particle
    private static final double PARTICLES_PER_BLOCK = (360 * ROUNDS_PER_BLOCK) / PARTICLE_DEGREE;
    private static final double PARTICLE_RADIUS = 1.0;
    private static final double RAD_INCREMENT = Math.toRadians(PARTICLE_DEGREE);
    private static final double HEIGHT_INCREMENT = 1.0 / PARTICLES_PER_BLOCK;
    private static final double COLUMN_OFFSET = PI_2 / COLUMN_COUNT; // nur bei "performant" nötig

    private double y = 0.0;
    private double rad = 0.0;

    public Shrine() {
    }

    public void playAnimation() {
        calcNextLoc();
    }

    private void calcNextLoc() {
        particleLoc = this.loc.clone().add(0.5, 0.0, 0.5);

        double x;
        double z;
        for (int n = 0; n < COLUMN_COUNT; n++) {
            x = PARTICLE_RADIUS * Math.cos(this.rad + (n * COLUMN_OFFSET));
            z = PARTICLE_RADIUS * Math.sin(this.rad + (n * COLUMN_OFFSET));

            particleLoc.getWorld().spawnParticle(Particle.SPELL_WITCH, particleLoc.clone().add(x, this.y, z), 0);
        }

        this.y = (this.y + HEIGHT_INCREMENT) % HEIGHT;
        this.rad = (this.rad + RAD_INCREMENT) % PI_2;
    }

}
