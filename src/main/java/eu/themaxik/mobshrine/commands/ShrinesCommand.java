package eu.themaxik.mobshrine.commands;

import eu.themaxik.mobshrine.util.Shrine;
import eu.themaxik.mobshrine.util.ShrineManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ShrinesCommand implements CommandExecutor {

    private ShrineManager shrineManager;

    public ShrinesCommand(ShrineManager shrineManager) {
        this.shrineManager = shrineManager;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            sendShrines(commandSender);
            return true;
        }
        Player p = (Player) commandSender;
        if (!p.isOp()) {
            p.sendMessage("§cNix Rechte");
            return true;
        }

        sendShrines(commandSender);

        return true;
    }

    private void sendShrines(CommandSender cs) {
        if (shrineManager.getShrines().isEmpty()) {
            cs.sendMessage("§cEs gibt keine Schreine");
            return;
        }

        for (Shrine shrine : shrineManager.getShrines()) {
            cs.sendMessage(shrine.getLoc().toString());
        }
    }

}
