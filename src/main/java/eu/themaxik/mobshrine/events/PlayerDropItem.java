/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.themaxik.mobshrine.events;

import eu.themaxik.mobshrine.Mobshrine;
import eu.themaxik.mobshrine.util.ShrineCreateEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

/**
 * @author Max Dizendorf
 */
public class PlayerDropItem implements Listener {

    private Mobshrine mobshrine;

    public PlayerDropItem(Mobshrine _mobshrine) {
        mobshrine = _mobshrine;
    }

    private HashMap<Item, Integer> taskid = new HashMap<>();

    @EventHandler
    public void playerDropItem(PlayerDropItemEvent event) {
        if (!event.isCancelled() && event.getItemDrop().getItemStack().getType() == Material.NETHER_STAR) {
            checkMovement(event.getItemDrop());
        }
    }

    private void checkMovement(Item item) {
        int id = new BukkitRunnable() {
            public void run() {
                if (item != null && !item.isDead()) {

                    Location loc = item.getLocation();
                    if (loc.getBlock().getRelative(0, -1, 0).getType() == Material.DRAGON_EGG) {
                        if (loc.getBlock().getRelative(0, -2, 0).getType() == Material.BEACON) {
                            Bukkit.getPluginManager().callEvent(new ShrineCreateEvent(item.getLocation().getBlock().getRelative(0, -2, 0).getLocation(), item));
                            cancelTask(item);
                        }
                    }

                    if (item.isOnGround()) {
                        cancelTask(item);
                    }
                } else {
                    cancelTask(item);
                }
            }
        }.runTaskTimer(mobshrine, 1, 5).getTaskId();
        taskid.put(item, id);

    }

    private void cancelTask(Item item) {
        if (taskid.containsKey(item)) {
            Bukkit.getScheduler().cancelTask(taskid.get(item));
            taskid.remove(item);
        }
    }


}
