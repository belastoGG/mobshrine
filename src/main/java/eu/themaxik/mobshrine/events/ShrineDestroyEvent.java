package eu.themaxik.mobshrine.events;

import eu.themaxik.mobshrine.util.Shrine;
import eu.themaxik.mobshrine.util.ShrineManager;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class ShrineDestroyEvent implements Listener {

    private ShrineManager shrineManager;

    public ShrineDestroyEvent(ShrineManager _shrineManager) {
        shrineManager = _shrineManager;
    }

    @EventHandler
    public void onPlayerBreakBlock(BlockBreakEvent event) {
        if (event.getBlock().getType() == Material.BEACON) {
            if (shrineManager.shrineExist(event.getBlock().getLocation())) {
                Shrine shrine = shrineManager.getShrine(event.getBlock().getLocation());
                shrineManager.unManage(shrine);
            }
        }
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
        for (Block b : event.blockList()) {
            if (b.getType() == Material.BEACON) {
                if (shrineManager.shrineExist(b.getLocation())) {
                    Shrine shrine = shrineManager.getShrine(b.getLocation());
                    shrineManager.unManage(shrine);
                }
            }
        }
    }


}
