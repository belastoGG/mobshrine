package eu.themaxik.mobshrine.events;

import eu.themaxik.mobshrine.Mobshrine;
import eu.themaxik.mobshrine.util.Shrine;
import eu.themaxik.mobshrine.util.ShrineCreateEvent;
import org.bukkit.*;
import org.bukkit.entity.EvokerFangs;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

public class PlayerCreatesShrineEvent implements Listener {

    private Mobshrine mobshrine;
    private int lifeTime;

    public PlayerCreatesShrineEvent(Mobshrine _mobshrine) {
        mobshrine = _mobshrine;
        lifeTime = mobshrine.getConfig().getInt("lifetime");
    }

    @EventHandler
    public void onShrineCreate(ShrineCreateEvent event) {
        Location loc = event.getLocation();
        if (mobshrine.shrineManager.shrineExist(event.getLocation())) {
            event.setCancelled(true);
            loc.getWorld().playEffect(loc.add(0, 1, 0), Effect.EXTINGUISH, 5);
            return;
        }

        if (event.getItem() != null) {
            event.getItem().remove();
        }

        playEffects(event.getLocation());

        Shrine shrine = new Shrine();
        shrine.loc = event.getLocation();
        shrine.endTime = System.currentTimeMillis() + getHoursInMilliseconds(this.lifeTime);
        shrine.randomName = UUID.randomUUID().toString().substring(10).replace("-", "");

        mobshrine.shrineManager.manage(shrine);

        for (double i = 0; i <= 50; i += 0.5) {
            event.getLocation().getWorld().spawnParticle(Particle.FLAME, event.getLocation().clone().add(0.5, i, 0.5), 1);
        }
        event.getLocation().getWorld().playSound(event.getLocation(),Sound.ENTITY_WITHER_AMBIENT,1,1);

    }


    private long getHoursInMilliseconds(int hours) {
        return hours * 3600000;
    }

    private void playEffects(Location loc) {
        World w = loc.getWorld();
        loc.getBlock().getRelative(0, 1, 0).setType(Material.AIR);
        w.spawn(loc.getBlock().getRelative(0, 1, 0).getLocation().add(0.5, 0.5, 0.5), EvokerFangs.class);
        w.playEffect(loc.getBlock().getRelative(0, 1, 0).getLocation().add(0.5, 0.5, 0.5), Effect.ENDER_SIGNAL, 1);
    }
}
