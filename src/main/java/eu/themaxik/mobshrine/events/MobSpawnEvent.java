package eu.themaxik.mobshrine.events;


import eu.themaxik.mobshrine.util.ShrineManager;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class MobSpawnEvent implements Listener {

    public ShrineManager shrineManager;

    public MobSpawnEvent(ShrineManager _shrineManager) {
        shrineManager = _shrineManager;
    }

    @EventHandler
    public void onMobSpawn(EntitySpawnEvent event) {
        if (event.getEntity() instanceof Monster) {
            if (shrineManager.isInReachOfShrine(event.getLocation())) {
                event.setCancelled(true);
            }
        }
    }
}
